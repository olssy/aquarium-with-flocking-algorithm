Travail pratique 1
==================

Étudiant
--------

Stéphane Olssen

Évaluation
----------

+-------------------------+----------------------------+-----------+-----------+
| Critère                 | Sous-critère               | Note      | Sur       |
+=========================+============================+===========+===========+
|                         | Scène                      | 10        | 10        |
|                         +----------------------------+-----------+-----------+
|                         | Gestion des obstacles      | 8         | 10        |
|                         +----------------------------+-----------+-----------+
|                         | Animation des sprites      | 10        | 10        |
|                         +----------------------------+-----------+-----------+
|                         | Cohésion                   | 15        | 15        |
| Fonctionnabilité        +----------------------------+-----------+-----------+
|                         | Alignement                 | 15        | 15        |
|                         +----------------------------+-----------+-----------+
|                         | Séparation                 | 15        | 15        |
|                         +----------------------------+-----------+-----------+
|                         | Mode *debug*               | 15        | 15        |
|                         +----------------------------+-----------+-----------+
|                         | Paramétrisation            | 5         | 15        |
+-------------------------+----------------------------+-----------+-----------+
|                         | Fichier README             | 10        | 10        |
|                         +----------------------------+-----------+-----------+
| Qualité de la remise    | Style de programmation     | 25        | 25        |
|                         +----------------------------+-----------+-----------+
|                         | Utilisation de Git         | 10        | 10        |
+-------------------------+----------------------------+-----------+-----------+
| Total                                                | 138       | 150       |
+-------------------------+----------------------------+-----------+-----------+

Commentaires
------------

- Graphiques jolis et originaux !
- Les trois heuristiques semblent bien fonctionnelles.
- La gestion des collisions pourrait être un peu mieux, car certains poissons
  semblent coincés sur les obstacles (cela semble partiellement réglé en
  changeant le mode avec N).
- Aucune paramétrisation ?
- Bonne structure de code.
- Bonne documentation.
- Utilisation adéquate de Git.
