## TP 1
## Auteur: Stephane Olssen
L'application se lance automatiquement.

*Touche P* - Pause la simulation ou la remettre en marche si elle est déjà en mode pause.

*Touche D* - Entre dans le mode "debug" ou en mode normale si déjà dans le mode "debug".

*Touche F* - Mode plein écran. EXPERIMENTAL, semble fonctionner mais emet une erreur sur la console.

*Touche K* - Diminuer la taille des poissons.

*Touche L* - Augmenter la taille des poissons.

*Touche B* - Allume/Ferme la génération de bulles des poissons.

*Touche N* - Change l'algorithme de séparation pour normaliser les vecteurs de différences ou pas.


Dans le mode Debug
Cliquer un  paramètre pour le modifier, entrer la nouvelle valeur et appuyer la touche "ENTER" pour modifier la valeur.
Cliquer sur un poisson pour afficher son menu debug. ensuite, sélectionner entre "Séparation", "Alignement" ou "Cohésion" pour afficher le mode debug du poisson.
