/*
 * Les parametres des poissons et du banc
 */
MyGame.Parametres = {

    nb_poissons: 50,
    poid_sep: 1,
    poid_al: 2,
    poid_co: 2,
    rayon_cercle_voisin_sep: 100,
    rayon_cercle_voisin_co: 100,
    rayon_cercle_voisin_al: 100,
    rayon_sep: 30,
    force_max: 0.05,
    vitesse_max: 1,
    echelle: 0.6

}