MyGame.Debug = function(game, banc) {

    this.game = game; // le jeu
    this.banc = banc; // le banc de poisson a debugger

    this.valeur_entree = ''; // valeur entree pour modifier un parametre
    MyGame.Debug.param_clicked = ''; // quelle parametre a modifer
    MyGame.Debug.mode_debug = false; // booleen pour verifier si on est en mode debuggage
    MyGame.Debug.type_debug = ''; // Le type de debuggage pour le poisson {sep, al, co}

    MyGame.Debug.normalize = false; // boolean pour savoir si on normalize les vecteur de separation

    // Titre du mode debuggage
    this.texte_debug_titre = this.game.add.text(0, 0, "MODE DEBUG", {
        align: "right",
        fill: "red"
    });
    this.texte_debug_titre.setTextBounds(0, 10, 800, 600);
    this.texte_debug_titre.boundsAlignH = "center";
    this.texte_debug_titre.visible = false;

    // Menu pour visualiser un poison
    this.texte_debug_sep = this.game.add.text(0, 0, "SEPARATION");
    this.texte_debug_sep.setTextBounds(800, 10, 780, 600);
    this.texte_debug_sep.boundsAlignH = "right";
    this.texte_debug_al = this.game.add.text(0, 30, "ALIGNEMENT");
    this.texte_debug_al.setTextBounds(800, 10, 780, 600);
    this.texte_debug_al.boundsAlignH = "right";
    this.texte_debug_co = this.game.add.text(0, 60, "COHESION");
    this.texte_debug_co.setTextBounds(800, 10, 780, 600);
    this.texte_debug_co.boundsAlignH = "right";

    this.texte_debug_sep.inputEnabled = true;
    this.texte_debug_co.inputEnabled = true;
    this.texte_debug_al.inputEnabled = true;

    this.texte_debug_sep.events.onInputDown.add(this.click_sep, this);
    this.texte_debug_al.events.onInputDown.add(this.click_al, this);
    this.texte_debug_co.events.onInputDown.add(this.click_co, this);

    this.texte_debug_sep.events.onInputOver.add(this.over_sep, this);
    this.texte_debug_al.events.onInputOver.add(this.over_al, this);
    this.texte_debug_co.events.onInputOver.add(this.over_co, this);

    this.texte_debug_sep.events.onInputOut.add(this.out_sep, this);
    this.texte_debug_al.events.onInputOut.add(this.out_al, this);
    this.texte_debug_co.events.onInputOut.add(this.out_co, this);

    // Menu de parametrage
    this.texte_params_nb_poissons = this.game.add.text(10, 50, "Nombre De Poissons: " + banc.poissons.length);
    this.texte_params_r_voisin_al = this.game.add.text(10, 90, "Rayon Voisin Alignement: " + MyGame.Parametres.rayon_cercle_voisin_al);
    this.texte_params_r_voisin_co = this.game.add.text(10, 120, "Rayon Voisin Cohesion: " + MyGame.Parametres.rayon_cercle_voisin_co);
    this.texte_params_r_voisin_sep = this.game.add.text(10, 150, "Rayon Voisin Separation: " + MyGame.Parametres.rayon_cercle_voisin_sep);
    this.texte_params_r_sep = this.game.add.text(10, 190, "Rayon Separation: " + MyGame.Parametres.rayon_sep);
    this.texte_params_vitesse_max = this.game.add.text(10, 220, "Vitesse Maximum: " + MyGame.Parametres.vitesse_max);
    this.texte_params_vitesse_angulaire_max = this.game.add.text(10, 250, "Vitesse Angulaire Maximum: " + MyGame.Parametres.force_max);
    this.texte_params_poid_co = this.game.add.text(10, 290, "Poid Cohesion: " + MyGame.Parametres.poid_co);
    this.texte_params_poid_al = this.game.add.text(10, 320, "Poid Alignement: " + MyGame.Parametres.poid_al);
    this.texte_params_poid_sep = this.game.add.text(10, 350, "Poid Separation: " + MyGame.Parametres.poid_sep);

    this.texte_params_nb_poissons.inputEnabled = true;
    this.texte_params_r_voisin_al.inputEnabled = true;
    this.texte_params_r_voisin_co.inputEnabled = true;
    this.texte_params_r_voisin_sep.inputEnabled = true;
    this.texte_params_r_sep.inputEnabled = true;
    this.texte_params_vitesse_max.inputEnabled = true;
    this.texte_params_vitesse_angulaire_max.inputEnabled = true;
    this.texte_params_poid_co.inputEnabled = true;
    this.texte_params_poid_al.inputEnabled = true;
    this.texte_params_poid_sep.inputEnabled = true;

    this.texte_params_nb_poissons.events.onInputDown.add(this.click_texte_params_nb_poissons, this);
    this.texte_params_r_voisin_al.events.onInputDown.add(this.click_texte_params_r_voisin_al, this);
    this.texte_params_r_voisin_co.events.onInputDown.add(this.click_texte_params_r_voisin_co, this);
    this.texte_params_r_voisin_sep.events.onInputDown.add(this.click_texte_params_r_voisin_sep, this);
    this.texte_params_r_sep.events.onInputDown.add(this.click_texte_params_r_sep, this);
    this.texte_params_vitesse_max.events.onInputDown.add(this.click_texte_params_vitesse_max, this);
    this.texte_params_vitesse_angulaire_max.events.onInputDown.add(this.click_texte_params_vitesse_angulaire_max, this);
    this.texte_params_poid_co.events.onInputDown.add(this.click_texte_params_poid_co, this);
    this.texte_params_poid_al.events.onInputDown.add(this.click_texte_params_poid_al, this);
    this.texte_params_poid_sep.events.onInputDown.add(this.click_texte_params_poid_sep, this);

    // cache les menu de debug
    this.cacherParams();
    this.cacherMenuPoisson();


    // Touche pour mode debug
    touche_debug = this.game.input.keyboard.addKey(Phaser.Keyboard.D);
    touche_debug.onDown.add(this.modeDebug, this);

    // Touche pour changer l'algo de separation pour normaliser ou non les vecteurs de differences
    this.texte_normalize = this.game.add.text(0, 0, "NORMALIZE", {
        align: "center",
        fill: "red"
    });
    this.texte_normalize.setTextBounds(0, 520, 800, 600);
    this.texte_normalize.boundsAlignH = "center";
    this.texte_normalize.visible = false;

    // touche pour changer l'ago de separation pour normailzer ou non
    touche_normalize = this.game.input.keyboard.addKey(Phaser.Keyboard.N);
    touche_normalize.onDown.add(this.basculerNormalize, this);


};

MyGame.Debug.prototype = {
    /*
     * Allume ou eteind le mode debuggage
     */
    modeDebug: function() {
        MyGame.Debug.mode_debug = !MyGame.Debug.mode_debug;
        if (MyGame.Debug.mode_debug === true) {
            this.colorierTexte();
            this.afficherTitre();
            this.afficherParams();
        } else {
            this.cacherTitre();
            this.cacherMenuPoisson();
            this.cacherParams();
        }
    },
    /*
     * Fonctions pour gerer le cliquage s'un item du menu de debuggage d'un poisson
     */
    click_sep: function() {

        this.texte_debug_sep.addColor('#FF0000', 0);
        this.texte_debug_co.addColor('#000000', 0);
        this.texte_debug_al.addColor('#000000', 0);


        MyGame.Debug.type_debug = 'sep';
    },
    click_al: function() {

        this.texte_debug_sep.addColor('#000000', 0);
        this.texte_debug_co.addColor('#000000', 0);
        this.texte_debug_al.addColor('#FF0000', 0);

        MyGame.Debug.type_debug = 'al';
    },
    click_co: function() {

        this.texte_debug_sep.addColor('#000000', 0);
        this.texte_debug_co.addColor('#FF0000', 0);
        this.texte_debug_al.addColor('#000000', 0);

        MyGame.Debug.type_debug = 'co';
    },
    /*
     * Fonctions pour gerer le changement de couleur du menu debuggage d'un poisson
     */
    over_sep: function() {
        if (!MyGame.Debug.type_debug === 'sep') {
            this.texte_debug_sep.addColor('#00FFFF', 0);
        }
    },
    out_sep: function() {
        if (!MyGame.Debug.type_debug === 'sep') {
            this.texte_debug_sep.addColor('#000000', 0);
        }
    },
    over_co: function() {
        if (!MyGame.Debug.type_debug === 'co') {
            this.texte_debug_co.addColor('#00FFFF', 0);
        }
    },
    out_co: function() {
        if (!MyGame.Debug.type_debug === 'co') {
            this.texte_debug_co.addColor('#000000', 0);
        }
    },
    over_al: function() {
        if (!MyGame.Debug.type_debug === 'al') {
            this.texte_debug_al.addColor('#00FFFF', 0);
        }
    },
    out_al: function() {
        if (!MyGame.Debug.type_debug === 'al') {
            this.texte_debug_al.addColor('#000000', 0);
        }
    },
    /*
     * Initialiser le menu de debuggage d'un poisson
     */
    initMenuPoisson: function() {

        this.texte_debug_sep.addColor('#000000', 0);
        this.texte_debug_al.addColor('#000000', 0);
        this.texte_debug_co.addColor('#000000', 0);
    },
    /*
     * Afficher le menu de debuggage d'un poisson
     */
    afficherMenuPoisson: function() {

        this.texte_debug_sep.visible = true;
        this.texte_debug_co.visible = true;
        this.texte_debug_al.visible = true;

    },
    /*
     * Cacher le menu pour debuger un poisson
     */
    cacherMenuPoisson: function() {
        this.texte_debug_sep.visible = false;
        this.texte_debug_co.visible = false;
        this.texte_debug_al.visible = false;
    },
    /*
     * Afficher le titre de la mode debug
     */
    afficherTitre: function() {
        this.texte_debug_titre.visible = true;
    },
    /*
     * Cacher le titre de la mode debug
     */
    cacherTitre: function() {
        this.texte_debug_titre.visible = false;
    },
    /*
     * Afficher le menu des parametres
     */
    afficherParams: function() {
        this.texte_params_nb_poissons.visible = true;
        this.texte_params_r_voisin_al.visible = true;
        this.texte_params_r_voisin_co.visible = true;
        this.texte_params_r_voisin_sep.visible = true;
        this.texte_params_r_sep.visible = true;
        this.texte_params_vitesse_max.visible = true;
        this.texte_params_vitesse_angulaire_max.visible = true;
        this.texte_params_poid_co.visible = true;
        this.texte_params_poid_al.visible = true;
        this.texte_params_poid_sep.visible = true;
    },
    /*
     * Cacher le menu des parametres
     */
    cacherParams: function() {
        this.texte_params_nb_poissons.visible = false;
        this.texte_params_r_voisin_al.visible = false;
        this.texte_params_r_voisin_co.visible = false;
        this.texte_params_r_voisin_sep.visible = false;
        this.texte_params_r_sep.visible = false;
        this.texte_params_vitesse_max.visible = false;
        this.texte_params_vitesse_angulaire_max.visible = false;
        this.texte_params_poid_co.visible = false;
        this.texte_params_poid_al.visible = false;
        this.texte_params_poid_sep.visible = false;
    },
    /*
     * Fonction callback pour le cliquage des items du menu de parmaetres
     * Defini quel item a ete clique et ajoute un listener de clavier
     */
    click_texte_params_nb_poissons: function() {
        this.valeur_entree = '';
        this.param_clicked = "nb_poisson";
        this.game.input.keyboard.addCallbacks(this, null, null, this.entreeNumerique);
        this.colorierTexte("nb_poisson");
    },
    click_texte_params_r_voisin_al: function() {
        this.valeur_entree = '';
        this.param_clicked = "voisin_al";
        this.game.input.keyboard.addCallbacks(this, null, null, this.entreeNumerique);
        this.colorierTexte("voisin_al");
    },
    click_texte_params_r_voisin_co: function() {
        this.valeur_entree = '';
        this.param_clicked = "voisin_co";
        this.game.input.keyboard.addCallbacks(this, null, null, this.entreeNumerique);
        this.colorierTexte("voisin_co");
    },
    click_texte_params_r_voisin_sep: function() {
        this.valeur_entree = '';
        this.param_clicked = "voisin_sep";
        this.game.input.keyboard.addCallbacks(this, null, null, this.entreeNumerique);
        this.colorierTexte("voisin_sep");
    },
    click_texte_params_r_sep: function() {
        this.valeur_entree = '';
        this.param_clicked = "rayon_sep";
        this.game.input.keyboard.addCallbacks(this, null, null, this.entreeNumerique);
        this.colorierTexte("rayon_sep");
    },
    click_texte_params_vitesse_max: function() {
        this.valeur_entree = '';
        this.param_clicked = "vitesse_max";
        this.game.input.keyboard.addCallbacks(this, null, null, this.entreeNumerique);
        this.colorierTexte("vitesse_max");
    },
    click_texte_params_vitesse_angulaire_max: function() {
        this.valeur_entree = '';
        this.param_clicked = "force_max";
        this.game.input.keyboard.addCallbacks(this, null, null, this.entreeNumerique);
        this.colorierTexte("force_max");
    },
    click_texte_params_poid_co: function() {
        this.valeur_entree = '';
        this.param_clicked = "poid_co";
        this.game.input.keyboard.addCallbacks(this, null, null, this.entreeNumerique);
        this.colorierTexte("poid_co");
    },
    click_texte_params_poid_al: function() {
        this.valeur_entree = '';
        this.param_clicked = "poid_al";
        this.game.input.keyboard.addCallbacks(this, null, null, this.entreeNumerique);
        this.colorierTexte("poid_al");
    },
    click_texte_params_poid_sep: function() {
        this.valeur_entree = '';
        this.param_clicked = "poid_sep";
        this.game.input.keyboard.addCallbacks(this, null, null, this.entreeNumerique);
        this.colorierTexte("poid_sep");
    },
    /*
     * Gerer la capture d'entree pour les valeurs des parametres
     * char: Le caractere entree
     */
    entreeNumerique: function(char) {
        // Saisir touches numeriques
        if ((char >= 0 || char <= 9 || char === '.') && char !== ' ') {
            if (char === '.' && this.valeur_entree === '') {
                this.valeur_entree = '0';
            }
            if (this.estNumerique(this.valeur_entree + char)) {
                this.valeur_entree = this.valeur_entree + char;
                this.mettreAJourAffichageParam();
            }
        }
        // Touche "ENTRER" 
        else if (this.game.input.keyboard.lastKey.keyCode === Phaser.Keyboard.ENTER) {
            if (this.valeur_entree !== '') {
                this.mettreAJourParam();
                this.mettreAJourAffichageParam();
            }
            this.valeur_entree = '';
            this.game.input.keyboard.addCallbacks(this, null, null, null);
            this.param_clicked = '';
            this.colorierTexte();
        }
        // backspace
        else if (this.game.input.keyboard.lastKey.keyCode === Phaser.Keyboard.BACKSPACE) {
            if (this.valeur_entree.length > 0) {
                this.valeur_entree = this.valeur_entree.substring(0, this.valeur_entree.length - 1);
                this.mettreAJourAffichageParam();
            }
        }
    },
    /*
     * Mettre a jour un parametre des poissons
     */
    mettreAJourParam() {

        switch (this.param_clicked) {
            case "nb_poisson":
                MyGame.Parametres.nb_poissons = this.valeur_entree;
                this.banc.setNbPoisson(this.valeur_entree);
                break;
            case "voisin_al":
                MyGame.Parametres.rayon_cercle_voisin_al = this.valeur_entree;
                break;
            case "voisin_co":
                MyGame.Parametres.rayon_cercle_voisin_co = this.valeur_entree;
                break;
            case "voisin_sep":
                MyGame.Parametres.rayon_cercle_voisin_sep = this.valeur_entree;
                break;
            case "rayon_sep":
                MyGame.Parametres.rayon_sep = this.valeur_entree;
                break;
            case "vitesse_max":
                MyGame.Parametres.vitesse_max = this.valeur_entree;
                break;
            case "force_max":
                MyGame.Parametres.force_max = this.valeur_entree;
                break;
            case "poid_co":
                MyGame.Parametres.poid_co = this.valeur_entree;
                break;
            case "poid_al":
                MyGame.Parametres.poid_al = this.valeur_entree;
                break;
            case "poid_sep":
                MyGame.Parametres.poid_sep = this.valeur_entree;
                break;
            default:
                console.log("default");
        }
    },
    /*
     * Mettre a jour l'affichage du menu des parametres
     */
    mettreAJourAffichageParam() {

        switch (this.param_clicked) {
            case "nb_poisson":
                this.texte_params_nb_poissons.setText("Nombre De Poissons: " + this.valeur_entree);
                break;
            case "voisin_al":
                this.texte_params_r_voisin_al.setText("Rayon Voisin Alignement: " + this.valeur_entree);
                break;
            case "voisin_co":
                this.texte_params_r_voisin_co.setText("Rayon Voisin Cohesion: " + this.valeur_entree);
                break;
            case "voisin_sep":
                this.texte_params_r_voisin_sep.setText("Rayon Voisin Separation: " + this.valeur_entree);
                break;
            case "rayon_sep":
                this.texte_params_r_sep.setText("Rayon Separation: " + this.valeur_entree);
                break;
            case "vitesse_max":
                this.texte_params_vitesse_max.setText("Vitesse Maximum: " + this.valeur_entree);
                break;
            case "force_max":
                this.texte_params_vitesse_angulaire_max.setText("Vitesse Angulaire Maximum: " + this.valeur_entree);
                break;
            case "poid_co":
                this.texte_params_poid_co.setText("Poid Cohesion: " + this.valeur_entree);
                break;
            case "poid_al":
                this.texte_params_poid_al.setText("Poid Alignement: " + this.valeur_entree);
                break;
            case "poid_sep":
                this.texte_params_poid_sep.setText("Poid Separation: " + this.valeur_entree);
                break;

        }
    },
    /*
     * Colorier le texte pour le menu des parametre depandant quelle item est selectionne
     * param: le parametre selectionner
     *
     */
    colorierTexte: function(param) {
        switch (param) {
            case "nb_poisson":
                this.texte_params_nb_poissons.addColor('#FF0000', 0);
                this.texte_params_r_voisin_al.addColor('#000000', 0);
                this.texte_params_r_voisin_co.addColor('#000000', 0);
                this.texte_params_r_voisin_sep.addColor('#000000', 0);
                this.texte_params_r_sep.addColor('#000000', 0);
                this.texte_params_vitesse_max.addColor('#000000', 0);
                this.texte_params_vitesse_angulaire_max.addColor('#000000', 0);
                this.texte_params_poid_co.addColor('#000000', 0);
                this.texte_params_poid_al.addColor('#000000', 0);
                this.texte_params_poid_sep.addColor('#000000', 0);
                break;
            case "voisin_al":
                this.texte_params_nb_poissons.addColor('#000000', 0);
                this.texte_params_r_voisin_al.addColor('#FF0000', 0);
                this.texte_params_r_voisin_co.addColor('#000000', 0);
                this.texte_params_r_voisin_sep.addColor('#000000', 0);
                this.texte_params_r_sep.addColor('#000000', 0);
                this.texte_params_vitesse_max.addColor('#000000', 0);
                this.texte_params_vitesse_angulaire_max.addColor('#000000', 0);
                this.texte_params_poid_co.addColor('#000000', 0);
                this.texte_params_poid_al.addColor('#000000', 0);
                this.texte_params_poid_sep.addColor('#000000', 0);
                break;
            case "voisin_co":
                this.texte_params_nb_poissons.addColor('#000000', 0);
                this.texte_params_r_voisin_al.addColor('#000000', 0);
                this.texte_params_r_voisin_co.addColor('#FF0000', 0);
                this.texte_params_r_voisin_sep.addColor('#000000', 0);
                this.texte_params_r_sep.addColor('#000000', 0);
                this.texte_params_vitesse_max.addColor('#000000', 0);
                this.texte_params_vitesse_angulaire_max.addColor('#000000', 0);
                this.texte_params_poid_co.addColor('#000000', 0);
                this.texte_params_poid_al.addColor('#000000', 0);
                this.texte_params_poid_sep.addColor('#000000', 0);
                break;
            case "voisin_sep":
                this.texte_params_nb_poissons.addColor('#000000', 0);
                this.texte_params_r_voisin_al.addColor('#000000', 0);
                this.texte_params_r_voisin_co.addColor('#000000', 0);
                this.texte_params_r_voisin_sep.addColor('#FF0000', 0);
                this.texte_params_r_sep.addColor('#000000', 0);
                this.texte_params_vitesse_max.addColor('#000000', 0);
                this.texte_params_vitesse_angulaire_max.addColor('#000000', 0);
                this.texte_params_poid_co.addColor('#000000', 0);
                this.texte_params_poid_al.addColor('#000000', 0);
                this.texte_params_poid_sep.addColor('#000000', 0);
                break;
            case "rayon_sep":
                this.texte_params_nb_poissons.addColor('#000000', 0);
                this.texte_params_r_voisin_al.addColor('#000000', 0);
                this.texte_params_r_voisin_co.addColor('#000000', 0);
                this.texte_params_r_voisin_sep.addColor('#000000', 0);
                this.texte_params_r_sep.addColor('#FF0000', 0);
                this.texte_params_vitesse_max.addColor('#000000', 0);
                this.texte_params_vitesse_angulaire_max.addColor('#000000', 0);
                this.texte_params_poid_co.addColor('#000000', 0);
                this.texte_params_poid_al.addColor('#000000', 0);
                this.texte_params_poid_sep.addColor('#000000', 0);
                break;
            case "vitesse_max":
                this.texte_params_nb_poissons.addColor('#000000', 0);
                this.texte_params_r_voisin_al.addColor('#000000', 0);
                this.texte_params_r_voisin_co.addColor('#000000', 0);
                this.texte_params_r_voisin_sep.addColor('#000000', 0);
                this.texte_params_r_sep.addColor('#000000', 0);
                this.texte_params_vitesse_max.addColor('#FF0000', 0);
                this.texte_params_vitesse_angulaire_max.addColor('#000000', 0);
                this.texte_params_poid_co.addColor('#000000', 0);
                this.texte_params_poid_al.addColor('#000000', 0);
                this.texte_params_poid_sep.addColor('#000000', 0);
                break;
            case "force_max":
                this.texte_params_nb_poissons.addColor('#000000', 0);
                this.texte_params_r_voisin_al.addColor('#000000', 0);
                this.texte_params_r_voisin_co.addColor('#000000', 0);
                this.texte_params_r_voisin_sep.addColor('#000000', 0);
                this.texte_params_r_sep.addColor('#000000', 0);
                this.texte_params_vitesse_max.addColor('#000000', 0);
                this.texte_params_vitesse_angulaire_max.addColor('#FF0000', 0);
                this.texte_params_poid_co.addColor('#000000', 0);
                this.texte_params_poid_al.addColor('#000000', 0);
                this.texte_params_poid_sep.addColor('#000000', 0);
                break;
            case "poid_co":
                this.texte_params_nb_poissons.addColor('#000000', 0);
                this.texte_params_r_voisin_al.addColor('#000000', 0);
                this.texte_params_r_voisin_co.addColor('#000000', 0);
                this.texte_params_r_voisin_sep.addColor('#000000', 0);
                this.texte_params_r_sep.addColor('#000000', 0);
                this.texte_params_vitesse_max.addColor('#000000', 0);
                this.texte_params_vitesse_angulaire_max.addColor('#000000', 0);
                this.texte_params_poid_co.addColor('#FF0000', 0);
                this.texte_params_poid_al.addColor('#000000', 0);
                this.texte_params_poid_sep.addColor('#000000', 0);
                break;
            case "poid_al":
                this.texte_params_nb_poissons.addColor('#000000', 0);
                this.texte_params_r_voisin_al.addColor('#000000', 0);
                this.texte_params_r_voisin_co.addColor('#000000', 0);
                this.texte_params_r_voisin_sep.addColor('#000000', 0);
                this.texte_params_r_sep.addColor('#000000', 0);
                this.texte_params_vitesse_max.addColor('#000000', 0);
                this.texte_params_vitesse_angulaire_max.addColor('#000000', 0);
                this.texte_params_poid_co.addColor('#000000', 0);
                this.texte_params_poid_al.addColor('#FF0000', 0);
                this.texte_params_poid_sep.addColor('#000000', 0);
                break;
            case "poid_sep":
                this.texte_params_nb_poissons.addColor('#000000', 0);
                this.texte_params_r_voisin_al.addColor('#000000', 0);
                this.texte_params_r_voisin_co.addColor('#000000', 0);
                this.texte_params_r_voisin_sep.addColor('#000000', 0);
                this.texte_params_r_sep.addColor('#000000', 0);
                this.texte_params_vitesse_max.addColor('#000000', 0);
                this.texte_params_vitesse_angulaire_max.addColor('#000000', 0);
                this.texte_params_poid_co.addColor('#000000', 0);
                this.texte_params_poid_al.addColor('#000000', 0);
                this.texte_params_poid_sep.addColor('#FF0000', 0);
                break;
            default:
                this.texte_params_nb_poissons.addColor('#000000', 0);
                this.texte_params_r_voisin_al.addColor('#000000', 0);
                this.texte_params_r_voisin_co.addColor('#000000', 0);
                this.texte_params_r_voisin_sep.addColor('#000000', 0);
                this.texte_params_r_sep.addColor('#000000', 0);
                this.texte_params_vitesse_max.addColor('#000000', 0);
                this.texte_params_vitesse_angulaire_max.addColor('#000000', 0);
                this.texte_params_poid_co.addColor('#000000', 0);
                this.texte_params_poid_al.addColor('#000000', 0);
                this.texte_params_poid_sep.addColor('#000000', 0);
        }
    },
    /*
     * Valider si une chaine en entree est une valeur numerique
     * nombre: la chaine a valider
     * retourne vrai si la chaine est numerique sinon faux
     */
    estNumerique: function(nombre) {
        return !isNaN(parseFloat(nombre)) && isFinite(nombre);
    },
    /*
     * Basculer le mode normalizer pour l'algo de separation
     */
    basculerNormalize: function() {
        MyGame.Debug.normalize = !MyGame.Debug.normalize;
        this.texte_normalize.visible = MyGame.Debug.normalize;
    }
};