MyGame.Game = function(game) {
    this.game = game;
    this.add;
    this.camera;
    this.cache;
    this.input;
    this.load;
    this.math;
    this.sound;
    this.stage;
    this.time;
    this.tweens;
    this.stage;
    this.world;
    this.particles;
    this.physics;
    this.rnd;
    this.Point;

    // les objets qui forment les limites
    this.mur_gauche;
    this.mur_droite;
    this.plafond;
    this.plancher;

    // le nom des sprite ainsi que le nombre d'images des animations
    this.poissons = ["poisson_1", "poisson_2"];
    this.nb_images = [10, 13];

    this.pause = false; // determine si la simulation est pause
}

MyGame.Game.prototype = {
    create: function() {
        // touche pour pause
        touche_pause = this.game.input.keyboard.addKey(Phaser.Keyboard.P);
        touche_pause.onDown.add(function() {
            this.pause = !this.pause;
            this.game.physics.arcade.isPaused = this.pause;
        }, this);

        // touche pour mode plein ecran
        touche_plein_ecran = this.game.input.keyboard.addKey(Phaser.Keyboard.F);
        touche_plein_ecran.onDown.add(function() {

            // logique pour mettre ne mode plein ecran ou mode normal
            this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
            if (this.game.scale.isFullScreen) {
                this.game.scale.stopFullScreen();
            } else {
                this.game.scale.startFullScreen(false);
            }
        }, this);

        // les sprites statiques
        this.add.sprite(0, 0, 'background');
        this.plancher = this.add.sprite(0, 570, 'plancher');
        this.plafond = this.add.sprite(0, 0, 'plafond');
        this.mur_gauche = this.add.sprite(0, 30, 'mur_g');
        this.mur_droite = this.add.sprite(770, 30, 'mur_d');
        this.coffre = this.add.sprite(350, 490, 'coffre');
        this.roche_1 = this.add.sprite(600, 470, 'roche');
        this.roche_2 = this.add.sprite(500, 440, 'roche');
        this.roche_3 = this.add.sprite(560, 400, 'roche');
        this.roche_4 = this.add.sprite(460, 410, 'roche');
        this.coffre.anchor.x = .5;
        this.coffre.anchor.y = .5;

        // charge l'engin de physique et activer la physique sur les sprites statiques
        this.physics.startSystem(Phaser.Physics.ARCADE);
        this.physics.arcade.enable([this.plancher, this.plafond, this.mur_gauche, this.mur_droite, this.coffre, this.roche_1, this.roche_2, this.roche_3, this.roche_4, this.plafond]);
        this.game.physics.setBoundsToWorld();

        // configurer la physique des sprites statiques
        this.plafond.enableBody = true;
        this.plancher.enableBody = true;
        this.mur_gauche.enableBody = true;
        this.mur_droite.enableBody = true;
        this.coffre.enableBody = true;
        this.roche_1.enableBody = true;
        this.roche_2.enableBody = true;
        this.roche_3.enableBody = true;
        this.roche_4.enableBody = true;

        this.mur_gauche.body.immovable = true;
        this.mur_droite.body.immovable = true;
        this.plafond.body.immovable = true;
        this.plancher.body.immovable = true;
        this.roche_1.body.immovable = true;
        this.roche_2.body.immovable = true;
        this.roche_3.body.immovable = true;
        this.roche_4.body.immovable = true;
        this.coffre.body.immovable = true;

        this.roche_2.scale.setTo(.8, .8);
        this.roche_3.scale.setTo(.6, .6);
        this.roche_4.scale.setTo(.4, .4);

        // les vecteurs de limites et obstacles pour la simulation
        this.obstacles = [this.coffre, this.roche_1, this.roche_2, this.roche_3, this.roche_4];
        this.limites = {
            haut: this.plafond,
            bas: this.plancher,
            gauche: this.mur_gauche,
            droite: this.mur_droite
        }

        // creer le banc
        this.banc = new MyGame.Banc(this, this.poissons, this.nb_images, this.obstacles, this.limites);

        // varable contenant le debuggag
        this.debug = new MyGame.Debug(this, this.banc);
    },
    // mettre a jour la simulation
    update: function() {
        // mettre a jour le banc
        this.banc.update(this.pause);
    }
};
