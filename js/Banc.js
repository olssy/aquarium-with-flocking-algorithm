MyGame.Banc = function(game, noms_sprites, nb_images, obstacles, limites) {

    this.game = game; // le jeu Phaser
    this.poissons = []; // vecteur de poissons dans le banc
    this.noms_sprites = noms_sprites; // vecteur des noms des sprites a utiliser 
    this.nb_images = nb_images; // vecteur du nombre d'images par sprite
    this.obstacles = obstacles; // vecteur de sprite obestacle
    this.limites = limites; // vecteur des sprite qui sert de limite de l'aquarium
    this.afficherBulles = false; // parametre pour savoir si ont doit afficher les bulles

    // ajouter les poissons initiale
    for (var i = 0; i < MyGame.Parametres.nb_poissons; i++) {
        this.addPoisson(new MyGame.Poisson(new Phaser.Point(this.game.world.centerX, this.game.world.centerY), game, noms_sprites[i % noms_sprites.length], nb_images[i % noms_sprites.length], obstacles, limites, this.poissons));
    }

    // event listener pour le clavier pour grossir/rapetisser un poisson
    touche_grossir = this.game.input.keyboard.addKey(Phaser.Keyboard.L);
    touche_grossir.onDown.add(this.grossirPoissons, this);

    touche_rapetisser = this.game.input.keyboard.addKey(Phaser.Keyboard.K);
    touche_rapetisser.onDown.add(this.rapetisserPoissons, this);

    // event listener pour basuler l'emission de bulles
    touche_bulles = this.game.input.keyboard.addKey(Phaser.Keyboard.B);
    touche_bulles.onDown.add(this.basculerBulles, this);
}

MyGame.Banc.prototype = {
    /*
     * Ajouter un poisson au vecteur des poissons
     */
    addPoisson: function(poisson) {
        this.poissons.push(poisson);
    },
    /*
     * Mettre a jour l'emplacement des poissons
     */
    update: function(pause) {

        // pour chaque poisson dans le banc, le mettre a jour
        for (var i = 0; i < this.poissons.length; i++) {

            poisson = this.poissons[i];
            poisson.update(this.poissons, pause);
        }

    },
    /*
     * Ajuster le nombre de poisson dans le banc
     * nombre: Le nombre de poisson du banc
     */
    setNbPoisson: function(nombre) {

        nb_poisson = this.poissons.length;

        // si on veux ajouter des poisson
        if (nombre > nb_poisson) {
            for (var i = 0; i < nombre - nb_poisson; i++) {

                // creer un nouveau poisson
                var poisson = new MyGame.Poisson(new Phaser.Point(this.game.world.centerX, this.game.world.centerY), this.game, this.noms_sprites[i % this.noms_sprites.length], this.nb_images[i % this.noms_sprites.length], this.obstacles, this.limites, this.poissons);
                // l'ajouter au banc
                this.addPoisson(poisson);
            }
        } else { // sinon on veux supprimer des poissons

            // detruire les poisons de trop
            for (var i = 0; i < nb_poisson - nombre; i++) {
                this.poissons[i].detruirePoisson();
                this.poissons[i].cacherAffichageDebug();
                MyGame.Debug.type_debug = '';
            }
            // ajouster la taille du vecteur de poissons dans le banc
            this.poissons.splice(0, nb_poisson - nombre);
        }
    },
    /*
     *  Grossir les poissons
     */
    grossirPoissons: function() {

        MyGame.Parametres.echelle = MyGame.Parametres.echelle * 1.1;

        // augemente leur taille de 10%
        for (var i = 0; i < this.poissons.length; i++) {
            this.poissons[i].changerEchelle(MyGame.Parametres.echelle);
        }
    },
    /*
     * Rapetisser les poisons
     */
    rapetisserPoissons: function() {

        MyGame.Parametres.echelle = MyGame.Parametres.echelle * 0.9;

        // diminue leur tailles de 10%
        for (var i = 0; i < this.poissons.length; i++) {
            this.poissons[i].changerEchelle(MyGame.Parametres.echelle);
        }
    },
    /*
     * Commencer ou arreter d'emettre des bulles
     */
    basculerBulles: function() {

        // basculer la varible decidant si on emmet des bulles
        this.afficherBulles = !this.afficherBulles;

        // si vrai creer des bulles
        if (this.afficherBulles === true) {

            for (var i = 0; i < this.poissons.length; i++) {
                this.poissons[i].creerBulles();
            }
        } else { //sinon detruire les bulles

            for (var i = 0; i < this.poissons.length; i++) {
                this.poissons[i].detruireBulles();
            }
        }
    }
};