var MyGame = {};

MyGame.Boot = function(game) {};

MyGame.Boot.prototype = {
    init: function() {

        this.input.maxPointers = 1; // une entree maximum(souris ou touch)

        // l'aligner horizontalement pour mode desktop
        if (this.game.device.desktop) {
            this.scale.pageAlignHorizontally = true;
        } else { // pour mobile
            this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL; // affiche le tout
            this.scale.setMinMax(400, 300, 800, 600); // resolution max et min
            this.scale.forceLandscape = true; // forcer en mode paysage
            this.scale.pageAlignHorizontally = true; // aligner horizontalement
        }
    },
    preload: function() {
        // barre de progression
        this.load.image('progression-bar', 'assets/progression-bar.png');

    },
    create: function() {
        this.state.start('preloader');
    }
};