MyGame.Poisson = function Poisson(emp, game, sprite_name, sprite_nb_images, obstacles, limites, voisins) {

    this.game = game;
    this.emplacement = emp.clone(); // l'emplacement du poisson
    this.voisins = voisins; // vecteurs de tous les poissons
    this.emetteurBulles; // l'emetteur des particules bulles

    // initialiser la velocite de depart du poisson aleatoirement
    xstart = Math.random() * 2 - 1;
    ystart = Math.random() * 2 - 1;
    this.velocite = new Phaser.Point(xstart, ystart);

    this.obstacles = obstacles; // les obstacles de la simulation
    this.limites = limites; // les limites de l'aquarium

    // initialiser le sprite du poison
    this.sprite = this.game.add.sprite(emp.x, emp.y, sprite_name);
    this.sprite.anchor.setTo(.5, .5);
    this.game.physics.arcade.enable(this.sprite);
    this.sprite.enableBody = true;
    this.sprite.animations.add('nage');
    this.sprite.animations.play('nage', sprite_nb_images, true);

    // Enregistrer les evenemnts de souris
    this.sprite.events.onInputDown.add(this.click, this);
    this.sprite.events.onInputOver.add(this.over, this);
    this.sprite.events.onInputOut.add(this.out, this);

    // initialiser les graphique de debuggage
    this.graphics_sep_cercle = this.game.add.graphics();
    this.graphics_sep_cercle.lineStyle(1, '0x009900');
    this.sep_cercle = this.graphics_sep_cercle.drawCircle(0, 0, MyGame.Parametres.rayon_sep * 2);

    this.graphics_point = this.game.add.graphics();
    this.graphics_point.lineStyle(3, '0xff0000');
    this.point = this.graphics_point.drawCircle(0, 0, 4);

    this.graphics_vec_sep = this.game.add.graphics();
    this.graphics_vec_sep.lineStyle(3, '0xFF0000');
    this.vec_sep = this.graphics_vec_sep.moveTo(0, 0);
    this.graphics_vec_sep.lineTo(50, 0);

    this.graphics_fleche_co = this.game.add.graphics();
    this.graphics_fleche_co.lineStyle(3, '0xFF0000');
    this.fleche_co = this.graphics_fleche_co.moveTo(0, 0);
    this.graphics_fleche_co.lineTo(40, 0);
    this.graphics_fleche_co.lineTo(30, 10);
    this.graphics_fleche_co.lineTo(40, 0);
    this.graphics_fleche_co.lineTo(30, -10);

    this.graphics_fleche_al_vec_dir = this.game.add.graphics();
    this.graphics_fleche_al_vec_dir.lineStyle(3, '0xFF0000');
    this.fleche_al_vec_dir = this.graphics_fleche_al_vec_dir.moveTo(0, 0);
    this.graphics_fleche_al_vec_dir.lineTo(-40, 0);
    this.graphics_fleche_al_vec_dir.lineTo(-30, 10);
    this.graphics_fleche_al_vec_dir.lineTo(-40, 0);
    this.graphics_fleche_al_vec_dir.lineTo(-30, -10);

    this.graphics_fleche_al_vec_dir_moy = this.game.add.graphics();
    this.graphics_fleche_al_vec_dir_moy.lineStyle(3, '0xFF00FF');
    this.fleche_al_vec_dir_moy = this.graphics_fleche_al_vec_dir_moy.moveTo(0, 0);
    this.graphics_fleche_al_vec_dir_moy.lineTo(40, 0);
    this.graphics_fleche_al_vec_dir_moy.lineTo(30, 10);
    this.graphics_fleche_al_vec_dir_moy.lineTo(40, 0);
    this.graphics_fleche_al_vec_dir_moy.lineTo(30, -10);

    this.graphics_voisin_cercle_sep = this.game.add.graphics();
    this.graphics_voisin_cercle_sep.lineStyle(1, '0x0000ff');
    this.voisin_cercle_sep = this.graphics_voisin_cercle_sep.drawCircle(0, 0, MyGame.Parametres.rayon_cercle_voisin_sep * 2);

    this.graphics_voisin_cercle_al = this.game.add.graphics();
    this.graphics_voisin_cercle_al.lineStyle(1, '0x0000ff');
    this.voisin_cercle_al = this.graphics_voisin_cercle_al.drawCircle(0, 0, MyGame.Parametres.rayon_cercle_voisin_al * 2);

    this.graphics_voisin_cercle_co = this.game.add.graphics();
    this.graphics_voisin_cercle_co.lineStyle(1, '0x0000ff');
    this.voisin_cercle_co = this.graphics_voisin_cercle_co.drawCircle(0, 0, MyGame.Parametres.rayon_cercle_voisin_co * 2);

    this.db_point;
    this.dbg_point = this.game.add.graphics();
    this.dbg_point.lineStyle(5, '0xffff00');
    this.db_point = this.dbg_point.drawCircle(0, 0, 4);

    this.sep_cercle.visible = false;
    this.voisin_cercle_sep.visible = false;
    this.voisin_cercle_co.visible = false;
    this.voisin_cercle_al.visible = false;
    this.vec_sep.visible = false;
    this.point.visible = false;
    this.fleche_co.visible = false;
    this.fleche_al_vec_dir_moy.visible = false;
    this.fleche_al_vec_dir.visible = false;

    // l'angle du poisson
    this.theta = 0;

    // la velocite des regles
    this.separation;
    this.alignement;
    this.cohesion;

    this.clicked = false; // si le poisson s'est fait "cliquer"

    this.acceleration; // vecteur d'acceleration

    this.changerEchelle(MyGame.Parametres.echelle); // regler l'echelle du sprite
}

MyGame.Poisson.prototype = {

    // mettre a jour le poisson
    update: function(voisins, pause) {

        this.voisins = voisins; // vecteur de tous les poissons dans le banc

        // arrete l'animation et rien faire si simulation en mode pause
        if (pause === true) {
            this.pauser();
        } else {
            // joue l'animation
            this.sprite.animations.play('nage');

            // change la couleur lorsque clique
            if (this.clicked === true) {
                this.sprite.tint = 0xFF0000;
            } else {
                this.sprite.tint = 0xFFFFFF;
            }

            // calcule l'acceleration
            this.acceleration = this.flock(this.voisins);

            // ajoute l'acceleration a la velocite
            this.velocite.add(this.acceleration.x, this.acceleration.y)

            // limiter les valeurs de velocite (bug dans Phaser.Point.clamp)
            this.velocite.x = Math.min(Math.max(this.velocite.x, -1 * MyGame.Parametres.vitesse_max), MyGame.Parametres.vitesse_max);
            this.velocite.y = Math.min(Math.max(this.velocite.y, -1 * MyGame.Parametres.vitesse_max), MyGame.Parametres.vitesse_max);

            // calcule l'angle de la velocite
            this.theta = (-1 * Math.atan2(-1 * this.velocite.y, this.velocite.x)) + (180 * Math.PI / 180);

            this.verifierDebug();

            // detecter les collisions avec les obstacles ou les limites
            this.eviterObstacles(this.obstacles);
            this.atteindreLimites(this.limites);

            this.deplacerPoisson();
        }
    },
    /*
     * Deplacer le poisson dans l'aquarium
     */
    deplacerPoisson: function() {
        // deplace le poisson par rapport a sa velocite
        this.emplacement.add(this.velocite.x, this.velocite.y)
        this.sprite.x = this.emplacement.x;
        this.sprite.y = this.emplacement.y;

        if (this.emetteurBulles !== undefined) {
            this.depacerBulles();
        }

        // assure que le poisson pointe toujours dans la direction de sa velocite
        if (this.velocite.x > 0) {
            this.sprite.scale.x = -Math.abs(this.sprite.scale.x);
            this.theta = this.theta - 2 * Math.PI / 2;
        } else {
            this.sprite.scale.x = Math.abs(this.sprite.scale.x);
        }
        // assure que le poisson pointe vers sa velocite
        this.sprite.rotation = this.theta;
    },
    /*
     * Verifier si ont est en mode debug et effectuer l'initialisation
     */
    verifierDebug: function() {

        // si mode debuggage alors mettre a jour les elements de debuggages
        if (MyGame.Debug.mode_debug === true) {
            //poisson clickable
            this.sprite.inputEnabled = true;

            this.deplacerDebug(MyGame.Debug.type_debug); // deplacement des elements de debuggage

            // si le poisson est selectionner alors assurer que ses elements de debuggage sont affiches
            if (this.clicked === true) {
                switch (MyGame.Debug.type_debug) {
                    case 'sep':
                        this.afficherSeparation();
                        break;
                    case 'co':
                        this.debug_cohesion();
                        break;
                    case 'al':
                        this.debug_alignement();
                        break;
                    default:
                        break;
                }
            }

        } else {
            // mode normal  
            this.cacherDebug();
        }
    },
    /*
     * Deplacer tous les elements graphiques de debuggage
     */
    deplacerDebug: function(mode) {

        // depandant du mode de debougage deplace les bons elements
        switch (mode) {
            case 'sep':
                this.deplacerDebugSeparation();
                break;
            case 'co':
                this.deplacerDebugCohesion();
                break;
            case 'al':
                this.deplacerDebugAlignement();
                break;
            default:
                break;
        }
    },
    /*
     * Cacher les elements graphiques de debuggages
     */
    cacherDebug: function() {

        // desactiver listener de click
        this.sprite.inputEnabled = false;

        // re-initialiser le type de debuggage
        MyGame.Debug.type_debug = '';

        // cacher les elements de debuggage
        this.sep_cercle.visible = false;
        this.voisin_cercle_sep.visible = false;
        this.voisin_cercle_co.visible = false;
        this.voisin_cercle_al.visible = false;
        this.vec_sep.visible = false;
        this.point.visible = false;
        this.fleche_co.visible = false;
        this.fleche_al_vec_dir_moy.visible = false;
        this.fleche_al_vec_dir.visible = false;
        this.sprite.tint = 0xFFFFFF;

    },
    /*
     * Calculer l'acceleration de poisson base sur l'algorithme de flocking
     */
    flock: function() {

        // calculer les forces qui define l'acceleration
        this.separation = this.separer().multiply(MyGame.Parametres.poid_sep, MyGame.Parametres.poid_sep);
        this.alignement = this.aligner().multiply(MyGame.Parametres.poid_al, MyGame.Parametres.poid_al);
        this.cohesion = this.cohesionner(true).multiply(MyGame.Parametres.poid_co, MyGame.Parametres.poid_co);

        // retourner la somme des 3 forces
        return Phaser.Point.add(Phaser.Point.add(this.alignement, this.separation), this.cohesion);

    },
    /*
     * Calculer la velocite pour se separer d'autres poissons
     */
    separer: function() {

        moyenne = new Phaser.Point();
        compteur = 0;

        // pour tous les poisson
        for (var i = 0; i < this.voisins.length; i++) {

            poisson = this.voisins[i];

            if (poisson == this) {
                continue;
            } // rien faire pour si c'est ce poisson
            // calculer la distance entre ce poisson et l'autre
            d = this.emplacement.distance(poisson.emplacement);

            // Si l'autre  poisson est dans le cercle de voisinage de spearation alors
            if (d > 0 && d < MyGame.Parametres.rayon_sep) {
                // trouve la difference des emplacements
                diff = Phaser.Point.subtract(this.emplacement, poisson.emplacement);
                // Puisque normaliser les veecteurs donne des bizarres de reultats avec les
                // limites on peux desativer la normalisation
                if (MyGame.Debug.normalize === true) {
                    diff.normalize();
                }
                diff.divide(d, d);

                // ajoutes le vecteur de difference au total des differences
                moyenne.add(diff.x, diff.y);

                compteur++;
            }
        }
        // Si il y a eu des poisson a se separer de alors
        if (compteur > 0) {
            // divise par le compteur pour avoir la moyenne
            moyenne.divide(compteur, compteur);
        }

        return moyenne;
    },
    /*
     * Calculer le vecteur pour s'aligner avec les autres poissons 
     */
    aligner: function() {

        moyenne = new Phaser.Point;
        compteur = 0;

        // pour tous les poissons
        for (var i = 0; i < this.voisins.length; i++) {

            poisson = this.voisins[i];

            if (poisson == this) {
                continue;
            } // rien faire pour si c'est ce poisson

            // cacule la distance entre ce pooisson et l'autre
            d = this.emplacement.distance(poisson.emplacement);

            // Si l'autre poisson est dans le cercle de voisinage de alignement alors
            if (d > 0 && d < MyGame.Parametres.rayon_cercle_voisin_al) {
                // ajoute sa velocite au total des velocite
                moyenne.add(poisson.velocite.x, poisson.velocite.y);
                compteur++;
            }
        }

        // Si il  y a 1 poisson ou plus dans le cercle d'alignenemt alors
        if (compteur > 0) {
            // trouve la moyenne des vecteurs
            moyenne.divide(compteur, compteur);
        }

        // limite la valeur a un maximum et un minimum
        moyenne.x = Math.min(Math.max(moyenne.x, -1 * MyGame.Parametres.force_max), MyGame.Parametres.force_max);
        moyenne.y = Math.min(Math.max(moyenne.y, -1 * MyGame.Parametres.force_max), MyGame.Parametres.force_max);

        return moyenne;
    },
    /*
     * Calculer le vecteur pour s'approcher des autres poissons 
     * deriger: Vrai si nous voulons limiter l'angle de deplacmenet
     */
    cohesionner: function(deriger = true) {

        somme = new Phaser.Point();
        compteur = 0;

        // pour tous les poissons
        for (var i = 0; i < this.voisins.length; i++) {

            poisson = this.voisins[i];

            if (poisson == this) {
                continue;
            } // rien faire pour si c'est ce poisson

            // Calculer la distance entre ce poisson et l'autre
            d = this.emplacement.distance(poisson.emplacement);

            // si l'autre poisson est dans le cercle de cohesion alors
            if (d > 0 && d < MyGame.Parametres.rayon_cercle_voisin_co) {
                // Ajoute l'emplacement de l'autre poisson au total des emplacement
                somme.add(poisson.emplacement.x, poisson.emplacement.y);
                compteur++;
            }
        }

        // Si il y a eu des poissons de trouve alors
        if (compteur > 0) {
            // calcule la moyenne des emplacements
            somme.divide(compteur, compteur);
            // soustrait l'emplacement de ce poison du resultat
            somme.subtract(this.emplacement.x, this.emplacement.y);

            // si nous voulons limiter l'angle de deplacement
            if (deriger === true) {
                somme = this.derigerVers(somme);
            }
        }

        return somme;
    },
    /*
     *  Limiter le deplacmenet a une vitesse maximum
     */
    derigerVers: function(cible) {

        // soustraire l'emplacement de ce poisson
        //desire = Phaser.Point.subtract(cible, this.emplacement);
        desire = cible;

        // calculer la magnitude(longueur) du vecteur
        d = desire.getMagnitude();

        // si la longueur est plus que 0 alors
        if (d > 0) {
            // normaliser le vecteur desire
            desire.normalize();

            // si la longueur est moins que 100 alors
            if (d < 100.0) {
                // multiplier le vecteur par la longueur divise par cent fois la vitesse maximum(ralenti le deplacment
                desire.multiply(MyGame.Parametres.vitesse_max * (d / 100.0), MyGame.Parametres.vitesse_max * (d / 100.0));
            } else {
                // multiple le vecteur par la vitesse maximum
                desire.multiply(MyGame.Parametres.vitesse_max, MyGame.Parametres.vitesse_max);
            }
            // soustrait la velocite de ce posson du vecteur desire
            derige = desire.subtract(this.velocite.x, this.velocite.y);
            // limite la vitesse par la force angulaire maximum
            derige.x = Math.min(Math.max(derige.x, -1 * MyGame.Parametres.force_max), MyGame.Parametres.force_max);
            derige.y = Math.min(Math.max(derige.y, -1 * MyGame.Parametres.force_max), MyGame.Parametres.force_max);
        }
        // si la longueur du vecteur est nul alors
        else {
            // on se derige nul part
            derige = new Phaser.Point(0, 0);
        }

        return derige;
    },
    /*
     *  Callback lorsque le poisson est clique
     */
    click: function(event, sprite) {
        // bascule la variable clicked
        this.clicked = !this.clicked;

        // si de-clique
        if (this.clicked === false) {
            // de-selectionner ce poisson 
            this.deSelectionnerTypeDebug()
        } else { // sinon
            // selectionner ce poisson
            this.selectionnerTypeDebug();
        }
    },
    /*
     *  Callback pour souris par dessus poisson
     */
    over: function(event, sprite) {
        // teinter le poisson rouge
        this.sprite.tint = 0xFF0000;
    },
    /*
     * Call back lorsque la sourise n'est plus sur le poisson
     */
    out: function(event, sprite) {
        // enlever le tint du poisson
        this.sprite.tint = 0xFFFFFF;
    },

    /*
     * Modifier la velocite pour eviter des obstacles
     *
     * obstacles: Array Les obstacles a eviter
     *
     */
    eviterObstacles: function(obstacles) {

        // pour tous les obstacles
        for (var obstacle in obstacles) {

            // point temporiairavec une velocite 5 fois plus que ce poisson
            temp = new Phaser.Point(this.velocite.x * 5, this.velocite.y * 5);
            //temp = new Phaser.Point(this.velocite.x+this.sprite.width, this.velocite.y+this.sprite.width); 

            // ce poisson la prochaine fois que sa velocite sera ajouter a son emplacmenet
            var cible = Phaser.Point.add(temp, this.emplacement);

            // si le poisson rentrera en collision avec l'obstacle alors
            if (obstacles[obstacle].body.hitTest(cible.x, cible.y)) {

                // inverse la direction du pooisson
                this.velocite.x = this.velocite.x * -1;
                this.velocite.y = this.velocite.y * -1;
            }
        }
    },

    /*
     * Modifier la velocite pour eviter de passer a travers les limites de l'aquarium 
     *
     * limites: Array les limites a ne pas franchir
     *
     */
    atteindreLimites: function(limites) {
        // pour tous les limites
        for (var limite in limites) {

            // calculer ou sera le poisson la prochaine deplacement
            var cible = Phaser.Point.add(this.velocite, this.emplacement);

            // si le poisson va atteindre la limite alors
            if (limites[limite].body.hitTest(cible.x, cible.y)) {
                // si c'est un limite haut/bas alors
                if (limite === "haut" || limite === "bas") {

                    // inverse la velocite sur l'axe des y
                    this.velocite.y = this.velocite.y * -1;

                }
                // sinon si c'est une limite gauche/droite alors
                else if (limite === "gauche" || limite === "droite") {

                    // inverse la velocite sur l'axe des x
                    this.velocite.x = this.velocite.x * -1;
                }
            }
        }
    },

    /*
     * Afficher les elements graphique pour la separation
     *
     */
    afficherSeparation: function() {
        for (var i = 0; i < this.voisins.length; i++) {
            this.voisins[i].cacherAffichageDebug();
        }

        this.voisin_cercle_al.visible = false;
        this.fleche_al_vec_dir_moy.visible = false;
        this.fleche_al_vec_dir.visible = false;

        this.fleche_co.visible = false;
        this.voisin_cercle_co.visible = false;

        this.voisin_cercle_sep.visible = true;
        this.sep_cercle.visible = true;
        this.vec_sep.visible = true;
    },

    /*
     * Afficher les elements graphique pour l'alignement
     *
     */
    debug_alignement: function() {
        for (var i = 0; i < this.voisins.length; i++) {
            this.voisins[i].cacherAffichageDebug();
        }

        this.fleche_co.visible = false;
        this.voisin_cercle_co.visible = false;

        this.voisin_cercle_sep.visible = false;
        this.sep_cercle.visible = false;
        this.vec_sep.visible = false;

        this.voisin_cercle_al.visible = true;
        this.fleche_al_vec_dir_moy.visible = true;
        this.fleche_al_vec_dir.visible = true;

        for (var i = 0; i < this.voisins.length; i++) {

            poisson = this.voisins[i];

            d = this.emplacement.distance(poisson.emplacement);

            if (d >= 0 && d < MyGame.Parametres.rayon_cercle_voisin_al) {
                poisson.fleche_al_vec_dir.visible = true;
            } else {
                poisson.fleche_al_vec_dir.visible = false;
            }
        }
    },

    /*
     * Afficher les elements graphique pour la cohesion
     *
     */
    debug_cohesion: function() {
        for (var i = 0; i < this.voisins.length; i++) {
            this.voisins[i].cacherAffichageDebug();
        }

        this.voisin_cercle_al.visible = false;
        this.fleche_al_vec_dir_moy.visible = false;
        this.fleche_al_vec_dir.visible = false;

        this.voisin_cercle_sep.visible = false;
        this.sep_cercle.visible = false;
        this.vec_sep.visible = false;

        this.fleche_co.visible = true;
        this.voisin_cercle_co.visible = true;

        for (var i = 0; i < this.voisins.length; i++) {

            poisson = this.voisins[i];

            d = this.emplacement.distance(poisson.emplacement);

            if (d >= 0 && d < MyGame.Parametres.rayon_cercle_voisin_co) {
                poisson.point.visible = true;
            } else {
                poisson.point.visible = false;
            }
        }
    },

    /*
     *  Selectionner le poisson pour debuggage
     *
     */
    selectionnerTypeDebug: function() {

        // s'assurrer qu'aucun autre poisson est selectionner 
        for (var i = 0; i < this.voisins.length; i++) {
            this.voisins[i].cacherAffichageDebug();
            this.voisins[i].clicked = false;
        }
        // afficher le menu de debuggage de poisson
        this.game.debug.afficherMenuPoisson();
        this.clicked = true;
    },

    /*
     *  De-selectionner le poisson
     *
     */
    deSelectionnerTypeDebug: function() {
        // cacher l'affichage de debuggage de tous les poissons
        for (var i = 0; i < this.voisins.length; i++) {
            this.voisins[i].cacherAffichageDebug();
        }

        this.clicked = false;

        // cacher les menu debug de poisson
        this.game.debug.initMenuPoisson();
        this.game.debug.cacherMenuPoisson();

        // re-initaliser la varaible pour le type de dubuggage
        MyGame.Debug.type_debug = '';
    },
    /*
     *  Detruit le poisson et ses elements graphiques
     */
    detruirePoisson: function() {
        this.sprite.kill();
        this.sep_cercle.kill();
        this.detruireBulles();
    },

    /*
     *  Cacher tous les elements graphique de debuggage
     *
     */
    cacherAffichageDebug: function() {
        if (this.voisins !== undefined) {
            for (var i = 0; i < this.voisins.length; i++) {

                this.voisins[i].sep_cercle.visible = false;
                this.voisins[i].voisin_cercle_sep.visible = false;
                this.voisins[i].voisin_cercle_co.visible = false;
                this.voisins[i].voisin_cercle_al.visible = false;
                this.voisins[i].vec_sep.visible = false;
                this.voisins[i].point.visible = false;
                this.voisins[i].fleche_co.visible = false;
                this.voisins[i].fleche_al_vec_dir_moy.visible = false;
                this.voisins[i].fleche_al_vec_dir.visible = false;
            }
        }
    },

    /*
     *  Deplacer les elements graphiques pour le dubuggage de la separation
     *
     */
    deplacerDebugSeparation: function() {

        // mouvement de la ligne de direction de separation
        this.graphics_vec_sep.x = this.sprite.x;
        this.graphics_vec_sep.y = this.sprite.y;

        // angle de la ligne de direction de separation
        if (this.separation.x !== 0 && this.separation.y !== 0) {
            this.graphics_vec_sep.angle = this.sprite.position.angle(Phaser.Point.add(this.separation, this.sprite.position), true);
        }
        // mouvement de la cercle de separation
        this.sep_cercle.x = this.sprite.x;
        this.sep_cercle.y = this.sprite.y;
        // rayon de la cercle de separation
        this.sep_cercle.height = MyGame.Parametres.rayon_sep * 2;
        this.sep_cercle.width = MyGame.Parametres.rayon_sep * 2;

        // mouvement des cercles cercles de voisinage
        this.voisin_cercle_sep.x = this.sprite.x;
        this.voisin_cercle_sep.y = this.sprite.y;

        this.voisin_cercle_sep.height = MyGame.Parametres.rayon_cercle_voisin_sep * 2;
        this.voisin_cercle_sep.width = MyGame.Parametres.rayon_cercle_voisin_sep * 2;

    },

    /*
     *  Deplacer les elements graphiques pour le dubuggage du cohesion
     *
     */
    deplacerDebugCohesion: function() {

        // mouvement de la cercle de voisinage
        this.point.x = this.sprite.x;
        this.point.y = this.sprite.y;

        // mouvement de la fleche de direction
        this.fleche_co.x = this.sprite.x;
        this.fleche_co.y = this.sprite.y;

        // rotation de la fleche de direction
        cohesion = this.cohesionner(this.voisins, false);
        if (cohesion.x !== 0 && cohesion.y !== 0) {
            this.graphics_fleche_co.angle = this.sprite.position.angle(cohesion.add(this.sprite.x, this.sprite.y), true);
        }
        // largeur de la cercle de voisinage
        this.voisin_cercle_co.height = MyGame.Parametres.rayon_cercle_voisin_co * 2;

        this.voisin_cercle_co.width = MyGame.Parametres.rayon_cercle_voisin_co * 2;

        // mouvement des cercles cercles de voisinage
        this.voisin_cercle_co.x = this.sprite.x;
        this.voisin_cercle_co.y = this.sprite.y;

    },
    /*
     *  Deplacer les elements graphiques pour le dubuggage de l'alignement
     *
     */
    deplacerDebugAlignement: function() {

        alignement = this.aligner(this.voisins);

        // mouvement de la fleche de direction moyenne
        this.fleche_al_vec_dir_moy.x = this.sprite.x;
        this.fleche_al_vec_dir_moy.y = this.sprite.y;

        // rotation de la fleche de direction moyenne
        if (alignement.x !== 0 && alignement.y !== 0) {
            this.fleche_al_vec_dir_moy.angle = this.sprite.position.angle(Phaser.Point.add(alignement, this.sprite.position), true);
        }

        // mouvement de la fleche de direction
        this.fleche_al_vec_dir.x = this.sprite.x;
        this.fleche_al_vec_dir.y = this.sprite.y;

        // rotation de la fleche de direction
        this.fleche_al_vec_dir.rotation = this.theta;

        // largeur de la cercle de voisinage
        this.voisin_cercle_al.height = MyGame.Parametres.rayon_cercle_voisin_al * 2;
        this.voisin_cercle_al.width = MyGame.Parametres.rayon_cercle_voisin_al * 2;

        // mouvement des cercles cercles de voisinage
        this.voisin_cercle_al.x = this.sprite.x;
        this.voisin_cercle_al.y = this.sprite.y;

    },
    /*
     *  Creer l'emetteur a bulle et la demarrer
     *
     */
    creerBulles: function() {

        // creer l'emetteur
        this.emetteurBulles = this.game.add.emitter(0, 0, 0);
        this.emetteurBulles.makeParticles('bulle');

        // pas de rotation
        this.emetteurBulles.setRotation(0, 0);

        // vitesse et direction des bulles
        this.emetteurBulles.minParticleSpeed.setTo(0, -0);
        this.emetteurBulles.maxParticleSpeed.setTo(0, -200);

        // faire grossir les bulles une fois creer
        this.emetteurBulles.setScale(0.01, .5, 0.01, .5, 16000, Phaser.Easing.Quintic.Out);

        // appliquer un peu de gravite vers le haut
        this.emetteurBulles.gravity = -10;

        // demarer l'emtteur
        this.emetteurBulles.start(false, 3000, 5000);
    },

    /*
     *  Detruit l'emetteur de bulle et ses particules
     *
     */
    detruireBulles: function() {

        // verife que l'emetteur est initialise
        if (this.emetteurBulles !== undefined) {

            // tuer les particules
            this.emetteurBulles.forEach(function(particle) {
                particle.kill();
            });

            // tuer l'emetteur et re-initialiser la variable
            this.emetteurBulles.kill();
            this.emetteurBulles = undefined;
        }
    },

    /*
     *  Deplacer l'emetteur de bulles pour que celui soit toujours
     *  centrer vers la bouche du poisson
     *
     */
    depacerBulles: function() {

        // chercher l'emplacement de la bouche du poisson en faisant une rotation autour du centre du poisson
        p = new Phaser.Point(this.emplacement.x - this.sprite.width / 2, this.emplacement.y);
        p.rotate(this.emplacement.x, this.emplacement.y, this.theta)
        this.emetteurBulles.x = p.x;
        this.emetteurBulles.y = p.y;

        // emet une bulle environs 1 fois sur 20
        if (Math.random() < 0.05) {
            this.emetteurBulles.forEach(function(particle) {
                if (particle.tinted !== true) {

                    // teinte la bulle aleatoirment
                    tint = Phaser.Color.getRandomColor(180, 255, 255);
                    particle.tint = tint;
                    particle.tinted = true;
                }
            });
            // emettre la bulle
            this.emetteurBulles.emitParticle(p.x, p.y, "bulle");
        }
    },
    pauser: function() {
        this.sprite.animations.stop('nage', null);
        if (this.emetteurBulles !== undefined) {
            this.emetteurBulles.on = false;
            this.emetteurBulles.forEach(function(particle) {
                particle.animations.stop();
            });
        }
    },
    depauser: function() {},
    changerEchelle: function(echelle) {
        this.sprite.scale.x = echelle;
        this.sprite.scale.y = echelle;
    }
};
