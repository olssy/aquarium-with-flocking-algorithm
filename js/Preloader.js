MyGame.Preloader = function(game) {
    this.preloadBar = null;
    this.ready = false;
};

MyGame.Preloader.prototype = {
    preload: function() {
        // pre-charger les assets
        this.preloadBar = this.add.sprite(this.world.centerX, this.world.centerY, 'progression-bar');
        this.preloadBar.anchor.setTo(0.5, 0.5);
        this.load.setPreloadSprite(this.preloadBar);
        this.load.image('background', 'assets/background.png');
        this.load.image('plafond', 'assets/plafond.png');
        this.load.image('plancher', 'assets/plancher.png');
        this.load.image('mur_g', 'assets/mur_g.png');
        this.load.image('mur_d', 'assets/mur_d.png');
        this.load.image('bulle', 'assets/bulle.png');
        this.load.image('coffre', 'assets/coffre-tresor.png');
        this.load.image('roche', 'assets/roche.png');
        this.load.spritesheet('poisson_1', 'assets/sprite_sheet_poisson_1_small.png', 45, 21);
        this.load.spritesheet('poisson_2', 'assets/sprite_sheet_poisson_2_small.png', 70, 40);
    },
    create: function() {
        this.preloadBar.cropEnabled = false; // ne pas "cropper" la bar de chargement
    },
    update: function() {
        this.state.start('game');
    }
};
